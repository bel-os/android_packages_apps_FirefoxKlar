LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
 
# Name of the APK to build
LOCAL_MODULE := Klar
LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := Klar

ifeq ($(TARGET_ARCH), arm64)
	MY_Arch	:= Aarch64
	MY_arch	:= aarch64
endif

klar_root	:= $(LOCAL_PATH)
klar_dir	:= app
klar_out	:= $(PWD)/$(OUT_DIR)/target/common/obj/APPS/$(LOCAL_MODULE)_intermediates
klar_build	:= $(klar_root)/$(klar_dir)/build
klar_apk	:= build/outputs/apk/klar$(MY_Arch)/release/$(klar_dir)-klar-$(MY_arch)-release-unsigned.apk

$(klar_root)/$(klar_dir)/$(klar_apk):
	rm -rf $(klar_root)/build && rm -rf $(klar_root)/$(klar_dir)/build && rm -rf $(klar_root)/.gradle
	cd $(klar_root) && ./gradlew assembleRelease

# Build all java files in the java subdirectory
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := $(klar_dir)/$(klar_apk)
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
 
# Tell it to build an APK
include $(BUILD_PREBUILT)
